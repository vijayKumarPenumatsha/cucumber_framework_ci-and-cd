Feature: Farmers Loan Programe
   
   Background:
      
       Given I am on the Farm Service Agency Portal URL "https://www.fsa.usda.gov/online-services/farm-plus/index"
 #------------------------------------------------------------------------------------------- 
 @Farmers_Loan_Process
  Scenario Outline: Farmers Loan Process Portal
  	When User Able To Click On Programes & Services Drpdown
    Then User Able To Navigate On Farm Bill Home
    And User Able To Click On Farm Loans
    And User Able To Click On Online Services
    And User Able To Click On FSAFarm
    And User Navigate To FSAFarm Login Page
    And User Enter The UserId <UserId>
	And User Enter The Password <Password>
    And User Click On Submit Button  
    And User Click On Create Account
    And User Click On Customer Radio Button
    And User Click On Continue Button
    And User Enter The Email Address <Email>
    And User Click On Submit
    And User Click On Update Account
    And User Click On Continue To Login Page
    And User Click On Manage Account Dropdown
    And User Navigate To Change Password
    And User Enter On The UserID <UserId>
    And User Enter The Old Password <Password>
    And User Enter The On The Password <Password>
    And User Click On Submit Label
  
  Examples: 
      |UserId|Password|Email|
      |username|password@123@|abc@gmail.com|

  @Check_The_Loan_Process
  Scenario: Check The Loan Process Website
    When User Able To Click On Online Services Drpdown
    Then User Navigate To Click On FSAFarm
    And User click On eLDP Link
    And User Click On Farmers.gov Link
    And User Click On Learn More About QLA Link
    And User Click On Crop Eligibulity Link
    And User Click On Producer Eligigbulity Link
    And User Click On Qualifying Disaster Events Link
    And User Click On Payment Calculations Link
    And User Click On Payment Limitations Link
    And User Click On Distribution Of Payments Link
    And User Click On Future Coverage Requirements Link
    
    @Read_The_PDF_File
    Scenario: Read The PDF File 
    When User Able To Click On Online Services Drpdown
    Then User Navigate To Click On FSAFarm
    
 