package com.cucumberFramework.pageActions;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.cucumberFramework.helper.WaitHelper;
import com.cucumberFramework.pageObjects.LoginLogoutPage;
import com.cucumberFramework.pageUtils.SeleniumUtils;

public class PageActions {
	
	private WebDriver driver;
    //WaitHelper waitHelper;
    SeleniumUtils SeleniumUtils = new SeleniumUtils();
    private LoginLogoutPage LoginLogoutPage = null; 
	
    public PageActions(WebDriver driver) {
		this.driver = driver;
		this.LoginLogoutPage = new LoginLogoutPage();
		PageFactory.initElements(driver, this.LoginLogoutPage);	
	}
    
	public void navigateUrl(String url) {
		SeleniumUtils.navigateToURL(driver,url);
	}
	
	public void enterUsername(String UserName) {
		SeleniumUtils.setValueToElement(driver,this.LoginLogoutPage.enterTheUsername, UserName);
	}
	
    public void enterOnPassword(String Password) {
    	SeleniumUtils.setValueToElement(driver,this.LoginLogoutPage.enterThePassword, Password);
	}

    public void clicksubmit() {
    	SeleniumUtils.clickElementbyWebElementWithOutSendKeys(driver,this.LoginLogoutPage.clickOnSubmit);
    }
	
	public void enterUserName(String userName){
		this.LoginLogoutPage.userName.sendKeys(userName);
	}
	
	public void enterPassword(String password){
		this.LoginLogoutPage.password.sendKeys(password);
	}
	
	public void clickLoginButton(){
		this.LoginLogoutPage.loginButton.click();
	}

	//////
	
	public void clickOnProgramesAndServicesDrpdown() {
		SeleniumUtils.clickElementbyWebElementWithOutSendKeys(driver,this.LoginLogoutPage.clickOnProgramesServices);
		
	}

	public void navigateOnFarmBillHome() {
		SeleniumUtils.clickElementbyWebElementWithOutSendKeys(driver,this.LoginLogoutPage.clickOnFarmBillHome);
		
	}

	public void clickOnFarmLoans() {
		SeleniumUtils.clickElementbyWebElementWithOutSendKeys(driver,this.LoginLogoutPage.clickOnFarmLoans);
		
	}

	public void clickOnOnlineServices() {
		SeleniumUtils.clickElementbyWebElementWithOutSendKeys(driver,this.LoginLogoutPage.clickOnOnlineServices);
		
	}

	public void clickOnFSAFarm() {
		SeleniumUtils.clickElementbyWebElementWithOutSendKeys(driver,this.LoginLogoutPage.clickOnFSAFarm);
		
	}

	public void navigateToFSAFarmLoginPage() {
		SeleniumUtils.clickElementbyWebElementWithOutSendKeys(driver,this.LoginLogoutPage.clickOnFSAFarmLoginPage);
		
	}

	public void enterTheUserId(String UserId) {
		SeleniumUtils.setValueToElement(driver,this.LoginLogoutPage.enterTheUserId, UserId);
		
	}

	public void userEnterThePassword(String Password) {
		SeleniumUtils.setValueToElement(driver,this.LoginLogoutPage.enterOnPassword, Password);
		
	}

	public void clickOnSubmitButton() {
		SeleniumUtils.clickElementbyWebElementWithOutSendKeys(driver,this.LoginLogoutPage.clickOnSubmitButton);
		
	}

	public void clickOnCreateAccount() {
		SeleniumUtils.clickElementbyWebElementWithOutSendKeys(driver,this.LoginLogoutPage.createAccount);
		
	}

	public void clickOnCustomerRadioButton() {
		SeleniumUtils.clickElementbyWebElementWithOutSendKeys(driver,this.LoginLogoutPage.CustomerRadioButton);
		
	}

	public void clickOnCuntinueButton() {
		SeleniumUtils.clickElementbyWebElementWithOutSendKeys(driver,this.LoginLogoutPage.cuntinueButton);
		
	}

	public void enterTheEmailAddress(String Email) {
		SeleniumUtils.setValueToElement(driver,this.LoginLogoutPage.emailAddress, Email);
		
	}

	public void clickOnSubmit() {
		SeleniumUtils.clickElementbyWebElementWithOutSendKeys(driver,this.LoginLogoutPage.submit);
		
	}

	public void clickOnUpdateAccount() {
		SeleniumUtils.clickElementbyWebElementWithOutSendKeys(driver,this.LoginLogoutPage.updateAccount);
		
	}

	public void clickOnContinueToLoginPage() {
		SeleniumUtils.clickElementbyWebElementWithOutSendKeys(driver,this.LoginLogoutPage.continueToLoginPage);
		
	}

	public void clickOnManageAccountDropdown() {
		SeleniumUtils.clickElementbyWebElementWithOutSendKeys(driver,this.LoginLogoutPage.manageAccountDropdown);
		
	}

	public void navigateToChangePassword() {
		SeleniumUtils.clickElementbyWebElementWithOutSendKeys(driver,this.LoginLogoutPage.changePassword);
		
	}

	public void enterOnTheUserID(String UserId) {
		SeleniumUtils.setValueToElement(driver,this.LoginLogoutPage.TheUserID, UserId);
		
	}

	public void enterTheOldPassword(String Password) {
		SeleniumUtils.setValueToElement(driver,this.LoginLogoutPage.OldPassword, Password);
		
	}

	public void enterTheOnThePassword(String Password) {
		SeleniumUtils.setValueToElement(driver,this.LoginLogoutPage.OnThePassword, Password);
		
	}

	public void clickOnSubmitLabel() {
		SeleniumUtils.clickElementbyWebElementWithOutSendKeys(driver,this.LoginLogoutPage.SubmitLabel);
		
	}

	public void clickOnOnlineServicesDrpdown() {
		SeleniumUtils.clickElementbyWebElementWithOutSendKeys(driver,this.LoginLogoutPage.OnlineServicesDrpdown);
		
	}

	public void navigateToClickOnFSAFarm() {
		SeleniumUtils.clickElementbyWebElementWithOutSendKeys(driver,this.LoginLogoutPage.FSAFarm);
		
	}

	public void clickOneLDPLink() {
		SeleniumUtils.clickElementbyWebElementWithOutSendKeys(driver,this.LoginLogoutPage.eLDP);
		
	}

	public void clickOnFarmersgovLink() {
		SeleniumUtils.clickElementbyWebElementWithOutSendKeys(driver,this.LoginLogoutPage.Farmersgov);
		
	}

	public void clickOnLearnMoreAboutQLALink() {
		SeleniumUtils.clickElementbyWebElementWithOutSendKeys(driver,this.LoginLogoutPage.LearnMoreAboutQLA);
		
	}

	public void clickOnCropEligibulityLink() {
		SeleniumUtils.clickElementbyWebElementWithOutSendKeys(driver,this.LoginLogoutPage.CropEligibulity);
		
	}

	public void clickOnProducerEligigbulityLink() {
		SeleniumUtils.clickElementbyWebElementWithOutSendKeys(driver,this.LoginLogoutPage.ProducerEligigbulity);
		
	}

	public void clickOnQualifyingDisasterEventsLink() {
		SeleniumUtils.clickElementbyWebElementWithOutSendKeys(driver,this.LoginLogoutPage.QualifyingDisasterEvents);
		
	}

	public void clickOnPaymentCalculationsLink() {
		SeleniumUtils.clickElementbyWebElementWithOutSendKeys(driver,this.LoginLogoutPage.PaymentCalculations);
		
	}

	public void clickOnPaymentLimitationsLink() {
		SeleniumUtils.clickElementbyWebElementWithOutSendKeys(driver,this.LoginLogoutPage.PaymentLimitations);
		
	}

	public void clickOnDistributionOfPaymentsLink() {
		SeleniumUtils.clickElementbyWebElementWithOutSendKeys(driver,this.LoginLogoutPage.DistributionOfPayments);
		
	}

	public void clickOnFutureCoverageRequirementsLink() {
		SeleniumUtils.clickElementbyWebElementWithOutSendKeys(driver,this.LoginLogoutPage.FutureCoverageRequirements);
		
	}
	
	public void readThePdfDocuments()
	{
		
	}

}
