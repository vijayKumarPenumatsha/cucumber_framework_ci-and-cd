package com.cucumberFramework.pageObjects;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.cucumberFramework.helper.WaitHelper;
import com.cucumberFramework.pageActions.PageActions;

public class LoginLogoutPage {
	
	//private WebDriver driver;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"email\"]")
	public WebElement enterTheUsername;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"pass\"]")
	public WebElement enterThePassword;
	
	@FindBy(how = How.XPATH, using = "/html/body/div[1]/div[2]/div[1]/div/div/div/div[2]/div/div[1]/form/div[2]/button")
	public WebElement clickOnSubmit;
	
	@FindBy(xpath="//input[@type='email']")
	public WebElement userName;
	
	@FindBy(xpath="//input[@id='continue']")
	public WebElement Continue;
	
	@FindBy(xpath="//input[@type='password']")
	public WebElement password;
	
	@FindBy(xpath="//input[@id='signInSubmit']")
	public WebElement loginButton;
	
	@FindBy(xpath="//div[@id='nav-tools']/a[@data-nav-role='signin']")
	public WebElement SignInfromNav;
	
	@FindBy(xpath="//span[contains(text(),'Sign')]/parent::a")
	public WebElement logoutBtn;
	
	@FindBy(xpath="//div[@id='nav-shop']/a")
	public WebElement allShopNav;

	@FindBy(xpath="//span[@data-nav-panelkey='TvApplElecPanel']")
	public WebElement TvApplElecPanel;
	
	@FindBy(xpath="//span[contains(text(),'Headphones')]/parent::a")
	public WebElement headPhonesCatLnk;
	
	@FindBy(xpath="//div[@id='mainResults']/ul/li[1]/div/div/div/a[contains(@class,'access-detail-page')]")
	public WebElement firstHeadPhoneLnk;
	
	@FindBy(xpath="//input[@id='add-to-cart-button']")
	public WebElement addToCartBtn;
	
	@FindBy(xpath="//a[@id='nav-cart']")
	public WebElement cartButton;
	
	@FindBy(xpath="//form[@id='activeCartViewForm']/div[@data-name='Active Items' or contains(@class,'sc-list-body')]//input[@value='Delete']")
	public List<WebElement> itemList;
	
	//form[@id='activeCartViewForm']/div[@data-name='Active Items' or contains(@class,'sc-list-body')]//input[@value='Delete']
	
	@FindBy(xpath="//div[contains(@class,'nav-search-field')]/input")
	public WebElement itemSearchField;
	
	@FindBy(xpath="//div[starts-with(@class,'sg-col-4')]/div[@class='sg-col-inner']/div/h5/a")
	public WebElement secondMacbookItem;
	
	@FindBy(xpath="//select[@id='quantity' or @name='quantity']")
	public List<WebElement> qtyField;
	
	///////
	
	@FindBy(xpath="//*[@id=\"primary-nav\"]/div/div/header/div/nav/ul/li[2]/a/button/span")
	public WebElement clickOnProgramesServices;
	
	@FindBy(xpath="//*[@id=\"basic-mega-nav-section-one\"]/div/div[2]/ul/li[5]/a")
	public WebElement clickOnFarmBillHome;
	
	@FindBy(xpath="//*[@id=\"side-nav\"]/div/div/div/div/nav/ul/li[7]/a")
	public WebElement clickOnFarmLoans;
	
	@FindBy(xpath="//*[@id=\"primary-footer\"]/div/div/div/nav/ul/li[4]/a/span")
	public WebElement clickOnOnlineServices;
	
	@FindBy(xpath="//*[@id=\"side-nav\"]/div/div/div/div/nav/ul/li[4]/a")
	public WebElement clickOnFSAFarm;
	
	@FindBy(xpath="//*[@id=\"main-content\"]/div/div/div/div/p[2]/a")
	public WebElement clickOnFSAFarmLoginPage;
	
	@FindBy(xpath="//*[@id=\"userId\"]")
	public WebElement enterTheUserId;
	
	@FindBy(xpath="//*[@id=\"password\"]")
	public WebElement enterOnPassword;
	
	@FindBy(xpath="//*[@id=\"main-content\"]/div/div/login/section/section[1]/div/div/div[1]/div[2]/div/form/div[2]/input")
	public WebElement clickOnSubmitButton;

	//////
	
	@FindBy(xpath="//*[@id=\"main-content\"]/div/div/login/section/section[1]/div/div/div[2]/a[1]/div")
	public WebElement createAccount;
	
	@FindBy(xpath="//*[@id=\"main-content\"]/div/div/registration/section/div/reg-determine-user-type/div/icam-field/div/div[3]/div/div/div[1]/div[1]/label")
	public WebElement CustomerRadioButton;
	
	@FindBy(xpath="//*[@id=\"submit-user-type-button\"]")
	public WebElement cuntinueButton;
	
	@FindBy(xpath="//*[@id=\"email\"]")
	public WebElement emailAddress;
	
	@FindBy(xpath="//*[@id=\"submit-email-button\"]")
	public WebElement submit;
	
	@FindBy(xpath="//*[@id=\"Update-Account-quicklink\"]")
	public WebElement updateAccount;
	
	@FindBy(xpath="//*[@id=\"custom-message\"]/dynamic-content/div/a")
	public WebElement continueToLoginPage;
	
	@FindBy(xpath="//*[@id=\"Manage-Account-dropdown-navlink\"]/span")
	public WebElement manageAccountDropdown;
	
	@FindBy(xpath="//*[@id=\"Change-Password-subnavlink\"]")
	public WebElement changePassword;
	
	@FindBy(xpath="//*[@id=\"username\"]")
	public WebElement TheUserID;
	
	@FindBy(xpath="//*[@id=\"oldPassword\"]")
	public WebElement OldPassword;
	
	@FindBy(xpath="//*[@id=\"password\"]")
	public WebElement OnThePassword;
	
	@FindBy(xpath="//*[@id=\"submit-button\"]")
	public WebElement SubmitLabel;
	
	////
	
	@FindBy(xpath="/html/body/div/div/div/div/div[3]/div/div/div/div/header/div/nav/ul/li[4]/a/button/span")
	public WebElement OnlineServicesDrpdown;
	
	@FindBy(xpath="//*[@id=\"basic-mega-nav-section-two\"]/div/div/ul/li[3]/a")
	public WebElement FSAFarm;
	
	@FindBy(xpath="//*[@id=\"side-nav\"]/div/div/div/div/nav/ul/li[3]/a")
	public WebElement eLDP;
	
	@FindBy(xpath="//*[@id=\"secondary-footer\"]/div/div/footer/div/div/div/div[1]/nav/div/div[1]/section/ul/li[1]/a")
	public WebElement Farmersgov;
	
	@FindBy(xpath="//*[@id=\"homepage-promo-left\"]/div/div[2]/a")
	public WebElement LearnMoreAboutQLA;
	
	@FindBy(xpath="//*[@id=\"accordion-2384\"]/h3/span")
	public WebElement CropEligibulity;
	
	@FindBy(xpath="//*[@id=\"accordion-2385\"]/h3/span")
	public WebElement ProducerEligigbulity;
	
	@FindBy(xpath="//*[@id=\"accordion-2386\"]/h3/span")
	public WebElement QualifyingDisasterEvents;
	
	@FindBy(xpath="//*[@id=\"accordion-2387\"]/h3/span")
	public WebElement PaymentCalculations;
	
	@FindBy(xpath="//*[@id=\"accordion-2388\"]/h3/span")
	public WebElement PaymentLimitations;
	
	@FindBy(xpath="//*[@id=\"accordion-2389\"]/h3/span")
	public WebElement DistributionOfPayments;
	
	@FindBy(xpath="//*[@id=\"accordion-2391\"]/h3/span")
	public WebElement FutureCoverageRequirements;
	
	
	
	
	
	
	
}
