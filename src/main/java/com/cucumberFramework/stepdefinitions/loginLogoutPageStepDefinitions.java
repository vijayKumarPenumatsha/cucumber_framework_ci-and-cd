package com.cucumberFramework.stepdefinitions;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.cucumberFramework.helper.WaitHelper;
import com.cucumberFramework.pageActions.PageActions;
import com.cucumberFramework.pageObjects.LoginLogoutPage;
import com.cucumberFramework.testBase.TestBase;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class loginLogoutPageStepDefinitions extends TestBase {

	public static PageActions pageActions = new PageActions(driver);
	//WaitHelper waitHelper = new WaitHelper(driver);
	
	@Given("^I am on the Farm Service Agency Portal URL \"([^\"]*)\"$")
	public void iAmOnTheFarmServiceAgencyPortalURL(String url) throws Throwable {
		Thread.sleep(5000);
		pageActions.navigateUrl(url);
		//waitHelper = new WaitHelper(driver);
	}
	
	@When("^User Able To Click On Programes & Services Drpdown$")
	public void userAbleToClickOnProgramesAndServicesDrpdown() throws Throwable {
		Thread.sleep(4000);
		pageActions.clickOnProgramesAndServicesDrpdown();
	}
	
	@Then("^User Able To Navigate On Farm Bill Home$")
	public void userAbleToNavigateOnFarmBillHome() throws Throwable {
		Thread.sleep(4000);
		pageActions.navigateOnFarmBillHome();
	}
	
	@And("^User Able To Click On Farm Loans$")
	public void userAbleToClickOnFarmLoans() throws Throwable {
		Thread.sleep(4000);
		pageActions.clickOnFarmLoans();
	}
	
	@And("^User Able To Click On Online Services$")
	public void userAbleToClickOnOnlineServices() throws Throwable {
		Thread.sleep(4000);
		pageActions.clickOnOnlineServices();
	}
	
	@And("^User Able To Click On FSAFarm$")
	public void userAbleToClickOnFSAFarm() throws Throwable {
		Thread.sleep(4000);
		pageActions.clickOnFSAFarm();
	}
	
	@And("^User Navigate To FSAFarm Login Page$")
	public void userNavigateToFSAFarmLoginPage() throws Throwable {
		Thread.sleep(4000);
		pageActions.navigateToFSAFarmLoginPage();
	}
	
	@And("^User Enter The UserId (.+)$")
	public void userEnterTheUserId(String UserId) throws Throwable {
		Thread.sleep(6000);
		pageActions.enterTheUserId(UserId);
	}
	
	@And("^User Enter The Password (.+)$")
	public void userEnterThePassword(String Password) throws Throwable {
		Thread.sleep(4000);
		pageActions.userEnterThePassword(Password);
	}
	
	@And("^User Click On Submit Button$")
	public void userClickOnSubmitButton() throws Throwable {
		Thread.sleep(4000);
		pageActions.clickOnSubmitButton();
	}
	
	@And("^User Click On Create Account$")
	public void userClickOnCreateAccount() throws Throwable {
		Thread.sleep(4000);
		pageActions.clickOnCreateAccount();
	}
	
	@And("^User Click On Customer Radio Button$")
	public void userClickOnCustomerRadioButton() throws Throwable {
		Thread.sleep(4000);
		pageActions.clickOnCustomerRadioButton();
	}
	
	@And("^User Click On Continue Button$")
	public void userClickOnCuntinueButton() throws Throwable {
		Thread.sleep(4000);
		pageActions.clickOnCuntinueButton();
	}
	
	@And("^User Enter The Email Address (.+)$")
	public void userEnterTheEmailAddress(String Email) throws Throwable {
		Thread.sleep(4000);
		pageActions.enterTheEmailAddress(Email);
	}
	
	@And("^User Click On Submit$")
	public void userClickOnSubmit() throws Throwable {
		Thread.sleep(4000);
		pageActions.clickOnSubmit();
	}
	
	@And("^User Click On Update Account$")
	public void userClickOnUpdateAccount() throws Throwable {
		Thread.sleep(7000);
		pageActions.clickOnUpdateAccount();
	}
	
	@And("^User Click On Continue To Login Page$")
	public void userClickOnContinueToLoginPage() throws Throwable {
		Thread.sleep(4000);
		pageActions.clickOnContinueToLoginPage();
	}
	
	@And("^User Click On Manage Account Dropdown$")
	public void userClickOnManageAccountDropdown() throws Throwable {
		Thread.sleep(7000);
		pageActions.clickOnManageAccountDropdown();
	}
	
	@And("^User Navigate To Change Password$")
	public void userNavigateToChangePassword() throws Throwable {
		Thread.sleep(4000);
		pageActions.navigateToChangePassword();
	}
	
	@And("^User Enter On The UserID (.+)$")
	public void userEnterOnTheUserID(String UserId) throws Throwable {
		Thread.sleep(4000);
		pageActions.enterOnTheUserID(UserId);
	}
	
	@And("^User Enter The Old Password (.+)$")
	public void userEnterTheOldPassword(String Password) throws Throwable {
		Thread.sleep(4000);
		pageActions.enterTheOldPassword(Password);
	}
	
	@And("^User Enter The On The Password (.+)$")
	public void userEnterTheOnThePassword(String Password) throws Throwable {
		Thread.sleep(4000);
		pageActions.enterTheOnThePassword(Password);
	}
	
	@And("^User Click On Submit Label$")
	public void userClickOnSubmitLabel() throws Throwable {
		Thread.sleep(3000);
		pageActions.clickOnSubmitLabel();
	}
	
	//
	
	@When("^User Able To Click On Online Services Drpdown$")
	public void userAbleToClickOnOnlineServicesDrpdown() throws Throwable {
		Thread.sleep(3000);
		pageActions.clickOnOnlineServicesDrpdown();
	}
	
	@Then("^User Navigate To Click On FSAFarm$")
	public void userNavigateToClickOnFSAFarm() throws Throwable {
		Thread.sleep(3000);
		pageActions.navigateToClickOnFSAFarm();
	}
	
	@And("^User click On eLDP Link$")
	public void userclickOneLDPLink() throws Throwable {
		Thread.sleep(3000);
		pageActions.clickOneLDPLink();
	}
	
	@And("^User Click On Farmers.gov Link$")
	public void userClickOnFarmersgovLink() throws Throwable {
		Thread.sleep(3000);
		pageActions.clickOnFarmersgovLink();
	}
	
	@And("^User Click On Learn More About QLA Link$")
	public void userClickOnLearnMoreAboutQLALink() throws Throwable {
		Thread.sleep(3000);
		pageActions.clickOnLearnMoreAboutQLALink();
	}
	
	@And("^User Click On Crop Eligibulity Link$")
	public void userClickOnCropEligibulityLink() throws Throwable {
		Thread.sleep(3000);
		pageActions.clickOnCropEligibulityLink();
	}
	
	@And("^User Click On Producer Eligigbulity Link$")
	public void userClickOnProducerEligigbulityLink() throws Throwable {
		Thread.sleep(3000);
		pageActions.clickOnProducerEligigbulityLink();
	}
	
	@And("^User Click On Qualifying Disaster Events Link$")
	public void userClickOnQualifyingDisasterEventsLink() throws Throwable {
		Thread.sleep(3000);
		pageActions.clickOnQualifyingDisasterEventsLink();
	}
	
	@And("^User Click On Payment Calculations Link$")
	public void userClickOnPaymentCalculationsLink() throws Throwable {
		Thread.sleep(3000);
		pageActions.clickOnPaymentCalculationsLink();
	}
	
	@And("^User Click On Payment Limitations Link$")
	public void userClickOnPaymentLimitationsLink() throws Throwable {
		Thread.sleep(3000);
		pageActions.clickOnPaymentLimitationsLink();
	}
	
	@And("^User Click On Distribution Of Payments Link$")
	public void userClickOnDistributionOfPaymentsLink() throws Throwable {
		Thread.sleep(3000);
		pageActions.clickOnDistributionOfPaymentsLink();
	}
	
	@And("^User Click On Future Coverage Requirements Link$")
	public void userClickOnFutureCoverageRequirementsLink() throws Throwable {
		Thread.sleep(3000);
		pageActions.clickOnFutureCoverageRequirementsLink();
	}
	
	
	
	//
	@When("^User Enter the Username (.+)$")
	public void enterTheUsername(String UserName) throws Throwable {
		Thread.sleep(4000);
		pageActions.enterUsername(UserName);
	}
	
	@And("^User Enter the Password (.+)$")
	public void enterThePassword(String Password) throws Throwable {
		Thread.sleep(4000);
		pageActions.enterOnPassword(Password);
	}
	
	@And("^User click on submit button$")
	public void clickOnSubmit() throws Throwable {
		Thread.sleep(4000);
		pageActions.clicksubmit();
	}
	
}