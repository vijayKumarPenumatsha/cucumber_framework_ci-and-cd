package com.cucumberFramework.pageUtils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.cucumberFramework.helper.WaitHelper;
import com.cucumberFramework.testBase.TestBase;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Reports {
	
	public static ExtentTest extentTest;
	public ExtentReports extentReport;
	public String genaratedReportName;
	private static Reports instance;
	public static Reports reports;
	public static WebDriver driver;
	
	public void intializeExtentreport()
	{
		String dateAndTime = new SimpleDateFormat("MM-dd-YYYY-HH-mm-ss").format(Calendar.getInstance().getTime());
		genaratedReportName = System.getProperty("target/html/ExtentReport11-" + dateAndTime + ".html");
		extentReport = new ExtentReports(genaratedReportName);
		extentReport.config().documentTitle("Automachine Report");
		extentReport.config().reportHeadline(dateAndTime);
		extentReport.config().reportName("Automated Regression Test");
		extentReport.addSystemInfo("Browser Name", "Chrome");
	}
	
	public static Reports getInstance()
	{
		if (instance == null) {
			instance = new Reports();
			instance.intializeExtentreport();
		}
		return instance;
	}
	
	public void verifyAndLog(String actual, String expected, String testCaseName)
	{
		if(actual.equals(expected)) {
			extentTest.log(LogStatus.PASS, testCaseName + " - Expected Value: " + expected + " , " + " Actual Value: " + actual);		
		} else {
			reportMatchFailError(testCaseName,expected,actual);
		}
	}
	
	public static void log(String text1, String text2, ExtentTest extentTest)
	{
		extentTest.log(LogStatus.PASS, text1 + " " + text2);
	}
	
	public static void logs(String text1, ExtentTest extentTest)
	{
		extentTest.log(LogStatus.PASS, text1);
	}
	
	public void closeTest(String scenarioName)
	{
		extentTest.log(LogStatus.PASS, "The Test Performed For : " + scenarioName + "is closed ");
	}
	
	public static void reportError(String message) {
		String screenShotPath = CaptureScreenshot(driver);
		if(extentTest != null) {
			String image = extentTest.addScreenCapture(screenShotPath);
			extentTest.log(LogStatus.FAIL,  message + image );
		}	
	}
	
	public static void reportInfo(String message) {
		String screenShotPath = CaptureScreenshot(driver);
		if(extentTest != null) {
			String image = extentTest.addScreenCapture(screenShotPath);
			extentTest.log(LogStatus.INFO,  message + image );
		}	
	}
	
	public void reportMatchFailError(String testCaseName, String expectedResult, String actualResult)
	{
		String screenShotPath = CaptureScreenshot(driver);
		if(extentTest != null) {
			String image = extentTest.addScreenCapture(screenShotPath);
			extentTest.log(LogStatus.FAIL,  testCaseName + ":-Actual Result: " + actualResult + ", Expected Result: " + expectedResult + " : Change Data" + image);
		}
	}
	
	public static String CaptureScreenshot(WebDriver driver)
	{
		String date = new SimpleDateFormat("dd-mm-yy").format(Calendar.getInstance().getTime());
		String dateAndTime = new SimpleDateFormat("dd-MM-yyyy hh-mm-ss a z").format(Calendar.getInstance().getTime());
		try {
			TakesScreenshot screenShot = (TakesScreenshot) driver;
			File source = screenShot.getScreenshotAs(OutputType.FILE);
			String dest = "c:\\Reports\\Screeenshots" + "_" + date + "\\Error_" + dateAndTime + ".png" ;
			File destination = new File(dest);
			FileUtils.copyFile(source, destination);
			return dest;
		} catch (IOException e) {
			return e.getMessage();
		}
	}
	
	public void verifyTrueAndLog(boolean expression, String testCaseName)
	{
		verifyAndLog(expression, true, testCaseName);
	}
	
	public void verifyFalseAndLog(boolean expression, String testCaseName)
	{
		verifyAndLog(expression, false, testCaseName);
	}
	
	public void verifyAndLog(boolean actual, boolean expected, String testCaseName)
	{
		if(actual == expected) {
			extentTest.log(LogStatus.PASS, testCaseName + " - Expected Valus: " + expected + ", " + " Actual Value: " + actual);
		} else {
			reportMatchFailError(testCaseName, expected, actual );
		}
	}
	
	public void reportMatchFailError(String testCaseName, boolean expectedResult, boolean actualResult)
	{
		String screenShotPath = CaptureScreenshot(driver);
		if(extentTest != null) {
			String image = extentTest.addScreenCapture(screenShotPath);
			extentTest.log(LogStatus.FAIL, testCaseName + ":-Actual Result: " + actualResult + ", Expected Result: " + expectedResult + " :Change Data" + image);
		}
	}

}
