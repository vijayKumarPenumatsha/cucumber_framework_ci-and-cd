package com.cucumberFramework.pageUtils;

import static org.junit.Assert.fail;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cucumberFramework.helper.WaitHelper;
import com.cucumberFramework.pageActions.PageActions;
import com.cucumberFramework.pageObjects.LoginLogoutPage;
import com.cucumberFramework.stepdefinitions.ServiceHooks;
import com.cucumberFramework.testBase.TestBase;
import com.relevantcodes.extentreports.ExtentReports;

public class SeleniumUtils {
	
	public static WebDriver driver;
	public static final int DRIVER_WAIT_TIME_IN_SECS = 40;
	public static final String ERROR_MSG = "Some error has occurred while performing operation::{}";
	private static final String DROPDOWN_ITEM_SELECTION_IN_OVERLAY = "//ul/li[*]/span[text()='%s']";
	private static final String DROPDOWN_PARTIAL_MATCH_ITEM_SELECTION_IN_OVERLAY = "//ul/li[*]/span[contains(text(),'%s')]";
	private static final String DROPDOWN_OVERLAY = "//ul";
	private static final String DROPDOWN_CLICKABLE_LABEL = "div/label";
	private static final Logger LOGGER = LoggerFactory.getLogger(SeleniumUtils.class);
	WaitHelper waitHelper;
	
	/**
	 * ----------------------------- Maximize Window -----------------------------------
	 */
	
	public static void maximizeWindow(final WebDriver driver) {
		driver.manage().window().maximize();
	}

	/**
	 * ----------------------------- Resize Window For Mobile -----------------------------------
	 */
	
	public static void resizeWindowForMobile(final WebDriver driver) {
		driver.manage().window().setSize(new Dimension(50,750));
	}

	/**
	 * ----------------------------- Resize Window For Tablet -----------------------------------
	 */
	
	public static void resizeWindowForTablet(final WebDriver driver) {
		driver.manage().window().setSize(new Dimension(700, 750));
	}

	/** 
	 * ----------------------------- Simple Date Format -----------------------------------
	 */
	
	public static String todayDate() {
		return new SimpleDateFormat("mm/dd/yyyy HH:mm").format(new Date());
	}
	
	/**
	 * -------------------------------------------------------------------
	 * Click on the WebElement With ID and handling WebDriverWait to handle NoSuchElementException
	 * Passing Element as String
	 */
	
	public static void clickElementByID(final WebDriver driver,final String elementID, final String elementName) {
		final WebDriverWait wait = new WebDriverWait(driver,DRIVER_WAIT_TIME_IN_SECS);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(elementID)));
		wait.until(ExpectedConditions.elementToBeClickable(By.id(elementID)));
		driver.findElement(By.id(elementID)).click();
	}
	
	/**
	 * -------------------------------------------------------------------
	 * Click on the WebElement With XPath and handling WebDriverWait to handle NoSuchElementException
	 * Passing Element as String
	 */
	
	public static void clickElementByXPath(final WebDriver driver, final String elementID, final String elementName) {
		try 
		{
		final WebDriverWait wait = new WebDriverWait(driver,DRIVER_WAIT_TIME_IN_SECS);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementID)));
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementID)));
		driver.findElement(By.xpath(elementID)).click();
		} catch (org.openqa.selenium.TimeoutException | NoSuchElementException ele) {
			LOGGER.error(ERROR_MSG, ele);
			fail();
		}
	}

	/**
	 * -------------------------------------------------------------------
	 * Click on the WebElement With XPath and handling WebDriverWait to handle NoSuchElementException
	 * Passing Element as WebElement
	 */
	
	public static void clickElementByWebElement(final WebDriver driver, final WebElement elementID, final String elementName) {
		try {
		final WebDriverWait wait = new WebDriverWait(driver,DRIVER_WAIT_TIME_IN_SECS);
		wait.until(ExpectedConditions.visibilityOf(elementID));
		wait.until(ExpectedConditions.elementToBeClickable(elementID));
		elementID.sendKeys("");
		elementID.click();
		} catch (org.openqa.selenium.TimeoutException | NoSuchElementException ele) {
			LOGGER.error(ERROR_MSG, ele);
			fail();
		}
		catch (final Exception e)
		{
			clickElementbyWebElementWithOutSendKeys(driver,elementID);
		}
	}
	
	/**
	 * -------------------------------------------------------------------
	 * Click on the WebElement With XPath and handling WebDriverWait to handle NoSuchElementException
	 * Passing Element as WebElement This Method doesn't use sendKeys
	 */
	
	public static void clickElementbyWebElementWithOutSendKeys(final WebDriver driver, final WebElement elementID) {
		try 
		{
		final WebDriverWait wait = new WebDriverWait(driver,DRIVER_WAIT_TIME_IN_SECS);
		wait.until(ExpectedConditions.visibilityOf(elementID));
		wait.until(ExpectedConditions.elementToBeClickable(elementID));
		elementID.click();
		} catch (org.openqa.selenium.TimeoutException | NoSuchElementException ele) {
			LOGGER.error(ERROR_MSG, ele);
			fail();
		} 
	}
	
	
	/**
	 * -------------------------------------------------------------------
	 * Submit Element by XPATH
	 */
	
	public static void submitElementbyXPath(final WebDriver driver, final String elementID, final String elmentNmae) {
		final JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", driver.findElement(By.xpath(elementID)));
	}
	
	/**
	 * -------------------------------------------------------------------
	 * Scroll till Web Element
	 */
	
	public static void scrollToWebElement(final WebDriver driver, final WebElement webElement) {
		final JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].scrollIntoView(true);",webElement );
	}
	
	/**
	 * -------------------------------------------------------------------
	 * Validate Element
	 */
	
	public static void validateElement(final WebDriver driver, final String xpath, final String testCaseNmae, final String expectedResult) {
		final WebDriverWait driverWait = new WebDriverWait(driver,DRIVER_WAIT_TIME_IN_SECS);
		try 
		{		
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		final WebElement element = driver.findElement(By.xpath(xpath));
		Assertions.assertEquals(expectedResult.trim(), element.getText().trim());
		//Reports.logs("Clicked On Element:" + elementName, Reports.extentTest);
		} catch (org.openqa.selenium.TimeoutException | NoSuchElementException ele) {
			//ele.printStackTrace();
			//Reports.reportError("Element - " + elementName + " . Timeout/No Such Element error - change Data");
			//return;
			LOGGER.error(ERROR_MSG, ele);
			fail();
		} 
	}
	
	/**
	 * -------------------------------------------------------------------
	 * Set value to Element
	 */
	
	public static void setValueToElementByXpath(final WebDriver driver, final String xpath, final String inputValue) 
	{
		final WebDriverWait driverWait = new WebDriverWait(driver,DRIVER_WAIT_TIME_IN_SECS);
		try {
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		final WebElement element = driver.findElement(By.xpath(xpath));
		element.clear();
		element.sendKeys(inputValue);
		} catch (org.openqa.selenium.TimeoutException | NoSuchElementException ele) {
			LOGGER.error(ERROR_MSG, ele);
			fail();
		} 
	}
	
	public static void setValueToElementByXpath(final WebDriver driver, final WebElement element, final String inputValue) 
	{
		final WebDriverWait driverWait = new WebDriverWait(driver,DRIVER_WAIT_TIME_IN_SECS);
		try {
		driverWait.until(ExpectedConditions.visibilityOf(element));
		element.clear();
		element.sendKeys(inputValue);
		} catch (org.openqa.selenium.TimeoutException | NoSuchElementException ele) {
			LOGGER.error(ERROR_MSG, ele);
			fail();
		} 
	}

	/**
	 * -------------------------------------------------------------------
	 * Set Value To WebElement using XPATH and handling WebDriverWait to handle NoSuchElementException
	 */
	
	public static void setValueToElement(final WebDriver driver, final WebElement webElement, final String inputValue) 
	{
		final WebDriverWait driverWait = new WebDriverWait(driver,DRIVER_WAIT_TIME_IN_SECS);
		try {
		driverWait.until(ExpectedConditions.visibilityOf(webElement));
		webElement.clear();
		webElement.sendKeys(inputValue);
		} catch (org.openqa.selenium.TimeoutException | NoSuchElementException ele) {
			LOGGER.error(ERROR_MSG, ele);
			fail();
		} 
	}

	/**
	 * -------------------------------------------------------------------
	 * get value by element
	 */
	
	public static String getVlaueByElement(final WebDriver driver, final WebElement webElement) 
	{
		final WebDriverWait driverWait = new WebDriverWait(driver,DRIVER_WAIT_TIME_IN_SECS);
		try {
		    if (driverWait.until(ExpectedConditions.visibilityOf(webElement)).isDisplayed()) {
		    	return webElement.getText().trim();
		    }
		} catch (org.openqa.selenium.TimeoutException | NoSuchElementException ele) {
			LOGGER.error(ERROR_MSG, ele);
			fail();
		} 
		return null;
	}
	
	/**
	 * -------------------------------------------------------------------
	 * Is element not empty
	 */
	
	public static boolean isElementNotEmpty(final WebDriver driver, final WebElement webElement, final String elementName) 
	{
		return StringUtils.isNotEmpty(getVlaueByElement(driver, webElement));
	}
	
	/**
	 * -------------------------------------------------------------------Get Text Value of
	 * the Input Element using XPATH and handling WebDriverWait to handle NoSuchElementException
	 */
	
	public static String getInputElementValue(final WebDriver driver, final WebElement webElement) 
	{
		final WebDriverWait driverWait = new WebDriverWait(driver,DRIVER_WAIT_TIME_IN_SECS);
		try {
		    if (driverWait.until(ExpectedConditions.visibilityOf(webElement)).isDisplayed()) {
		    	return webElement.getAttribute("value").trim();
		    }
		} catch (org.openqa.selenium.TimeoutException | NoSuchElementException ele) {
			LOGGER.error(ERROR_MSG, ele);
			fail();
		} 
		return null;
	}
	
	/**
	 * -------------------------------------------------------------------
	 * get Enable Disable by element
	 */
	
	public static Boolean getEnableDisableByElement(final WebDriver driver, final WebElement webElement) 
	{
		final WebDriverWait driverWait = new WebDriverWait(driver,DRIVER_WAIT_TIME_IN_SECS);
		try {
		    if (driverWait.until(ExpectedConditions.visibilityOf(webElement)) != null) {
		    	return webElement.isEnabled();
		    }
		} catch (org.openqa.selenium.TimeoutException | NoSuchElementException ele) {
			LOGGER.error(ERROR_MSG, ele);
			fail();
		} 
		return false;
	}
	
	/**
	 * -------------------------------------------------------------------
	 * is Disable by element
	 */
	
	public static Boolean isDisableByElement(final WebDriver driver, final WebElement webElement) 
	{
		final WebDriverWait driverWait = new WebDriverWait(driver,DRIVER_WAIT_TIME_IN_SECS);
		try {
		    if (driverWait.until(ExpectedConditions.visibilityOf(webElement)) != null) {
		    	final String attribute = webElement.getAttribute("disabled");
		    	return attribute != null && attribute.equalsIgnoreCase("true");
		    }
		} catch (org.openqa.selenium.TimeoutException | NoSuchElementException ele) {
			LOGGER.error(ERROR_MSG, ele);
			fail();
		} 
		return false;
	}
	
	/**
	 * -----------------------------------------------------------------------------------------------
	 * this method checks if the input's enabled status has changed by overriding the apply method and
	 * applying the condition that we are looking for pass testIsEnabled true if checking whether the
	 * input has become enabled pass testIsEnabled false if checking whether the input has become
	 * disabled 
	 */
	
	public static Boolean isEnableDisabledWaitingForChange(final WebDriver driver, final WebElement webElement, final Boolean testIsEnabled) 
	{
		final WebDriverWait driverWait = new WebDriverWait(driver,DRIVER_WAIT_TIME_IN_SECS);
		Boolean isEnabled = null;
		try {
			isEnabled = driverWait.until(driver1 -> {
		    	final Boolean isElementEnabled = webElement.isEnabled();
		    	return testIsEnabled ? isElementEnabled : isElementEnabled;
		    });
		} catch (org.openqa.selenium.TimeoutException ele) {
			LOGGER.error(ERROR_MSG, ele);
			fail();
		} 
		return isEnabled;
	}
	
	/**
	 * -------------------------------------------------------------------
	 * is Spinner Enabled
	 */
	
	public static Boolean isSpinnerEnabled(final WebDriver driver, final WebElement webElement) 
	{
		final WebDriverWait driverWait = new WebDriverWait(driver,DRIVER_WAIT_TIME_IN_SECS);
		try {
			if (driverWait.until(ExpectedConditions.visibilityOf(webElement)) != null) {
		    	final String webElementCSS = getElementsCSS(driver, webElement, "webelement CSS");
		    	if (webElementCSS != null && !webElementCSS.isEmpty()) {
		    		return webElementCSS.contains("loadingSpinner");
		    	} else {
		    		return false;
		    	}
		    }
		} catch (org.openqa.selenium.TimeoutException | NoSuchElementException ele) {
			LOGGER.error(ERROR_MSG, ele);
			fail();
		} 
		return false;
	}
	
	/**
	 * -------------------------------------------------------------------
	 * Is Disable By element CSS
	 */
	
	public static Boolean isDisabledByElementCSS(final WebDriver driver, final WebElement webElement) 
	{
		final WebDriverWait driverWait = new WebDriverWait(driver,DRIVER_WAIT_TIME_IN_SECS);
		try {
			if (driverWait.until(ExpectedConditions.visibilityOf(webElement)) != null) {
		    	final String webElementCSS = getElementsCSS(driver, webElement, "webelement CSS");
		    	if (webElementCSS != null && !webElementCSS.isEmpty()) {
		    		return webElementCSS.contains("ui-state-disabled");
		    	} else {
		    		return false;
		    	}
		    }
		} catch (org.openqa.selenium.TimeoutException | NoSuchElementException ele) {
			LOGGER.error(ERROR_MSG, ele);
			fail();
		} 
		return false;
	}
	
	/**
	 * -------------------------------------------------------------------
	 * Mouse over Element
	 */
	
	public static void mouseOverElement(final WebDriver driver, final WebElement webElement) 
	{
		final WebDriverWait driverWait = new WebDriverWait(driver,DRIVER_WAIT_TIME_IN_SECS);
		try {
		driverWait.until(ExpectedConditions.visibilityOf(webElement));
		final Actions actObj = new Actions(driver);
		actObj.moveToElement(webElement).build().perform();
		} catch (org.openqa.selenium.TimeoutException | NoSuchElementException ele) {
			LOGGER.error(ERROR_MSG, ele);
			fail();
		} 
	}
	
	/**
	 * -------------------------------------------------------------------
	 * check & uncheck box
	 */
	
	public static void clickCheckedUnCheckedElement(final WebDriver driver, final WebElement elementID) 
	{
		final WebDriverWait driverWait = new WebDriverWait(driver,DRIVER_WAIT_TIME_IN_SECS);
		try {
		driverWait.until(ExpectedConditions.visibilityOf(elementID));
		driverWait.until(ExpectedConditions.elementToBeClickable(elementID));
		final boolean chk = elementID.isEnabled();
		if(chk == true) {
			elementID.click();
		}
		} catch (org.openqa.selenium.TimeoutException | NoSuchElementException ele) {
			LOGGER.error(ERROR_MSG, ele);
			fail();
		} 
	}
	
	/**
	 * -------------------------------------------------------------------
	 * Mouse over Element select
	 */
	
	public static void mouseOverElementSelect(final WebDriver driver, final WebElement webElement) 
	{
		final WebDriverWait driverWait = new WebDriverWait(driver,DRIVER_WAIT_TIME_IN_SECS);
		try {
		driverWait.until(ExpectedConditions.visibilityOf(webElement)).isSelected();
		final Actions actObj = new Actions(driver);
		actObj.moveToElement(webElement).build().perform();
		} catch (org.openqa.selenium.TimeoutException | NoSuchElementException ele) {
			LOGGER.error(ERROR_MSG, ele);
			fail();
		} 
	}
	
	/**
	 * ----------------------------- Navigate To URL -----------------------------------
	 */
	
	public static void navigateToURL(final WebDriver driver, final String url) {
		driver.get(url);
	}
	
	/**
	 * ----------------------------- Reload Page -----------------------------------
	 */
	
	public static void reloadPage(final WebDriver driver)
	{
		driver.navigate().refresh();
	}
	
	/**
	 * ----------------------------- Navigate to URL in New Tab -----------------------------------
	 */
	
	public static void navigateToURLInNewTab(final WebDriver driver, final String url) {
		driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "t");
		final ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(0));
		driver.get(url);
	}
	
	/**
	 * -------------------------------------------------------------------
	 * Is Element present
	 */
	
	public static boolean isElementPresent(final WebDriver driver, final String xPath) 
	{
		try {
		  return ((ArrayList<String>) driver.findElement(By.xpath(xPath))).size() > 0;
		} catch (final org.openqa.selenium.NoSuchElementException ele) {
			LOGGER.error(ERROR_MSG, ele);
			fail();
			return false;
		} 
	}
	
	/**
	 * -------------------------------------------------------------------
	 * Is Anchor present
	 */
	
	public static boolean isAnchorPresent(final WebDriver driver, final String text) 
	{
		return SeleniumUtils.isElementPresent(driver, "//a[contains(text(), '" + text + "')]");
	}
	
	/**
	 * -------------------------------------------------------------------
	 * get CSS classes by element
	 */
	
	public static String getCssClassesByElement(final WebDriver driver, final WebElement webElement, final String elementName) 
	{
		final WebDriverWait driverWait = new WebDriverWait(driver,DRIVER_WAIT_TIME_IN_SECS);
		try {
			if(driverWait.until(ExpectedConditions.visibilityOf(webElement)).isDisplayed()) {
				return webElement.getAttribute("class").trim();
			}
		} catch (org.openqa.selenium.TimeoutException | NoSuchElementException ele) {
			LOGGER.error(ERROR_MSG, ele);
			fail();
		} 
		return null;
	}
	
	/**
	 * -------------------------------------------------------------------
	 * get CSS Value by element
	 */
	
	public static String getCssValueByElement(final WebDriver driver, final WebElement webElement, final String cssAttributeName, final String elementName) 
	{
		final WebDriverWait driverWait = new WebDriverWait(driver,DRIVER_WAIT_TIME_IN_SECS);
		try {
			if(driverWait.until(ExpectedConditions.visibilityOf(webElement)).isDisplayed()) {
				return webElement.getCssValue(cssAttributeName);
			}
		} catch (org.openqa.selenium.TimeoutException | NoSuchElementException ele) {
			LOGGER.error(ERROR_MSG, ele);
			fail();
		} 
		return null;
	}
	
	/**
	 * -------------------------------------------------------------------
	 * get data table size by XPath
	 */
	
	public static int getDataTableSizeByXpath(final WebDriver driver, final String tableXPath, final String testCaseName) 
	{
		final List<WebElement> rows = driver.findElements(By.xpath(tableXPath + "/tbody/tr"));
		return rows.size();
	}
	
	/**
	 * -------------------------------------------------------------------
	 * get mobile view card size by XPath
	 */
	
	public static int getMobileViewCardSizeByXpath(final WebDriver driver, final String cardXPath, final String testCaseName) 
	{
		final List<WebElement> rows = driver.findElements(By.xpath(cardXPath));
		return rows.size();
	}
	
	/**
	 * -------------------------------------------------------------------
	 * To Switch to a different window
	 */
	
	public static void switchToOtherWindow(final WebDriver driver, final int windowNumber) 
	{
		final List<String> browserTabs = new ArrayList<>(driver.getWindowHandles());
		driver.switchTo().window(browserTabs.get(windowNumber));
	}
	
	/**
	 * -------------------------------------------------------------------
	 * To Switch to a Multiple windows
	 */
	
	public static void switchToTheMultipleTabs(final WebDriver driver, final String windowNumber) 
	{
		driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + windowNumber);
	}
	
	/**
	 * -------------------------------------------------------------------
	 * Is Element Displayed
	 */
	
	public static boolean isElementDisplayed(final WebElement webElement) 
	{
		try {
		  return webElement.isDisplayed();
		} catch (final NoSuchElementException ele) {
			LOGGER.error(ERROR_MSG, ele);
			fail();
			return false;
		} 
	}
	
	/**
	 * -------------------------------------------------------------------
	 * Is Element not Displayed
	 */
	
	public static boolean isElementNotDisplayed(final WebElement webElement) 
	{
		try {
		  return !webElement.isDisplayed();
		} catch (final NoSuchElementException ele) {
			return false;
		} 
	}
	
	/**
	 * -------------------------------------------------------------------
	 * get element's CSS
	 */
	
	public static String getElementsCSS(final WebDriver driver, final WebElement webElement, final String elementName) 
	{
		final WebDriverWait driverWait = new WebDriverWait(driver,DRIVER_WAIT_TIME_IN_SECS);
		try {
			if (driverWait.until(ExpectedConditions.visibilityOf(webElement)).isDisplayed()) {
		    	return 	webElement.getAttribute("class");
		    }
		} catch (org.openqa.selenium.TimeoutException | NoSuchElementException ele) {
			LOGGER.error(ERROR_MSG, ele);
			fail();
		} 
		return null;
	}
	
	/**
	 * -------------------------------------------------------------------
	 * get web element from string path
	 */
	
	public static WebElement getWebElementFromStringPath(final WebDriver driver, final String xpath) 
	{
		return driver.findElement(By.xpath(xpath));
	}
	
	/**
	 * -------------------------------------------------------------------
	 * XPath Exists
	 */
	
	public static boolean xPathExists(final WebDriver driver, final String xpath) 
	{
		final WebDriverWait driverWait = new WebDriverWait(driver, 0);
		try {
			driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		    return true;   
		} catch (org.openqa.selenium.TimeoutException | NoSuchElementException ele) {
			LOGGER.error(ERROR_MSG, ele);
			fail();
			return false;
		} 		
	}
	
	/**
	 * -------------------------------------------------------------------
	 * get value by element no log
	 */
	
	public static String getValueByElementNoLog(final WebDriver driver, final WebElement webElement, final String elementName) 
	{
		final WebDriverWait driverWait = new WebDriverWait(driver,DRIVER_WAIT_TIME_IN_SECS);
		try {
		    if (driverWait.until(ExpectedConditions.visibilityOf(webElement)).isDisplayed()) {
		    	return webElement.getText().trim();
		    }
		} catch (org.openqa.selenium.TimeoutException | NoSuchElementException ele) {
			LOGGER.error(ERROR_MSG, ele);
			fail();
		} 
		return null;
	}
	
	/**
	 * -------------------------------------------------------------------
	 * check visibility by element
	 */
	
	public static Boolean checkVisibilityByElement(final WebDriver driver, final WebElement webElement, final String elementName) 
	{
		Boolean isVisible = false;
		final WebDriverWait driverWait = new WebDriverWait(driver,DRIVER_WAIT_TIME_IN_SECS);
		try {
		    if (driverWait.until(ExpectedConditions.visibilityOf(webElement)) != null) {
		    	isVisible = true;
		    }
		} catch (org.openqa.selenium.TimeoutException | NoSuchElementException ele) {
			LOGGER.error(ERROR_MSG, ele);
			fail();
		} 
		return isVisible;
	}
	
	/**
	 * -------------------------------------------------------------------
	 * Is Element Enabled
	 */
	
	public static Boolean isElementEnabled(final WebDriver driver, final WebElement webElement, final String elementName) 
	{
		Boolean isEnabled = false;
		final WebDriverWait driverWait = new WebDriverWait(driver,DRIVER_WAIT_TIME_IN_SECS);
		try {
		    if (driverWait.until(ExpectedConditions.visibilityOf(webElement)) != null) {
		    	isEnabled = webElement.isEnabled();
		    }
		} catch (org.openqa.selenium.TimeoutException | NoSuchElementException ele) {
			LOGGER.error(ERROR_MSG, ele);
			fail();
		} 
		return isEnabled;
	}
	
	/**
	 * -------------------------------------------------------------------
	 * Is Element not present
	 */
	
	public static Boolean isElementNotPresent(final WebDriver driver, final String xPath) 
	{
		final WebDriverWait driverWait = new WebDriverWait(driver,DRIVER_WAIT_TIME_IN_SECS);
		try {
		  return driverWait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(xPath)));
		} catch (final Exception ele) {
			LOGGER.error(ERROR_MSG, ele);
			fail();
			return true;
		} 
	}
	
	/**
	 * -------------------------------------------------------------------
	 * Is Element not present with wait
	 */
	
	public static Boolean isElementNotPresentWithWait(final WebDriver driver, final String xPath, final int waitTimeinSec) 
	{
		final WebDriverWait driverWait = new WebDriverWait(driver, waitTimeinSec);
		try {
		  return driverWait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(xPath)));
		} catch (final Exception ele) {
			LOGGER.error(ERROR_MSG, ele);
			fail();
			return true;
		} 
	}
	
	/**
	 * -------------------------------------------------------------------
	 * Is Button Enabled
	 */
	
	public static Boolean isButtonEnabled(final WebDriver driver, final WebElement webElement, final String elementName) 
	{
		final WebDriverWait driverWait = new WebDriverWait(driver,DRIVER_WAIT_TIME_IN_SECS);
		try {
		    if (driverWait.until(ExpectedConditions.visibilityOf(webElement)).isDisplayed()) {
		    	return webElement.isEnabled();
		    }
		} catch (org.openqa.selenium.TimeoutException | NoSuchElementException ele) {
			LOGGER.error(ERROR_MSG, ele);
			fail();
		} 
		return null;
	}
	
	/**
	 * -------------------------------------------------------------------
	 * get value by XPath
	 */
	
	public static String getValueByXpath(final WebDriver driver, final String xPath) 
	{
		final WebDriverWait driverWait = new WebDriverWait(driver,DRIVER_WAIT_TIME_IN_SECS);
		try {
		    if (driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPath))).isDisplayed()) {
		    	final WebElement webElement = driver.findElement(By.xpath(xPath));
		    	return webElement.getText().trim();
		    }
		} catch (org.openqa.selenium.TimeoutException | NoSuchElementException ele) {
			LOGGER.error(ERROR_MSG, ele);
			fail();
		} 
		return null;
	}
	
	/**
	 * -------------------------------------------------------------------
	 * is form control input field filled and valid
	 */
	
	public static boolean isFormControlInputFieldFilledAndValid(final WebDriver driver, final WebElement webElement) 
	{
		final String fieldCSS = SeleniumUtils.getElementsCSS(driver, webElement, "tsFailedPaymentOopsImage");
		return fieldCSS.contains("ui-state-filled") && !fieldCSS.contains("ng-invalid");
	}
	
	/**
	 * -------------------------------------------------------------------
	 * This method checks the value in list and return boolean result
	 */
	
	public static boolean checkValueInList(final WebDriver driver, final List<WebElement> webElement, final String expectedValueInList, final String elementName) 
	{
		final Boolean isValueExist = false;
		final WebDriverWait driverWait = new WebDriverWait(driver,DRIVER_WAIT_TIME_IN_SECS);
		try {
		   if (driverWait.until(ExpectedConditions.visibilityOfAllElements(webElement)) != null) {
			   List<WebElement> elements = webElement;
			   for(int i = 0; i < elements.size(); i++) {
				   final String listValue = elements.get(i).getText().trim();
				   if(listValue.contains(expectedValueInList)) {
					   return true;
				   }
			   }
			   return isValueExist;
		  }
		} catch (org.openqa.selenium.TimeoutException | NoSuchElementException ele) {
			LOGGER.error(ERROR_MSG, ele);
			fail();
		} 
		return isValueExist;
	}
	
	/**
	 * -------------------------------------------------------------------
	 * get URL of new tab
	 */
	
	public static String getUrlOfNewTab(final WebDriver driver, final int driverWaitTimeInSecs) 
	{
		final WebDriverWait driverWait = new WebDriverWait(driver, driverWaitTimeInSecs);
		//get Window Handlers as list
		final List<String> browserTabs = new ArrayList<>(driver.getWindowHandles());
		//ship to new tab and check if correct page opened
		String instructionsTitle = null;
		driver.switchTo().window(browserTabs.get(2));
		if(driverWait.until(ExpectedConditions.titleContains("Main Page"))) {
			instructionsTitle = driver.getTitle();
		}
		driver.switchTo().window(browserTabs.get(0));
		return instructionsTitle;
	}
	
	/**
	 * -------------------------------------------------------------------
	 * is text there
	 */
	
	public static String isTextThere(final WebDriver driver, final WebElement element, final Boolean testIsEnabled, final String elementName, final String attributeName) 
	{
		final WebDriverWait driverWait = new WebDriverWait(driver,DRIVER_WAIT_TIME_IN_SECS);
		try {
		    driverWait.until(ExpectedConditions.visibilityOf(element));
		    driverWait.until(ExpectedConditions.elementToBeClickable(element));
		    Thread.sleep(3000);
		    return driverWait.until(drivers -> {
		    	if(element.getAttribute(attributeName).length() > 0) {
		    		element.click();
		    		return element.getAttribute(attributeName);
		    	}
		    	return null;
		    });		    
		} catch (org.openqa.selenium.TimeoutException | InterruptedException ele) {
			LOGGER.error(ERROR_MSG, ele);
			fail();
		} 
		return null;
	}
	
	/**
	 * -------------------------------------------------------------------
	 * get element value by ID
	 */
	
	public static String getElementValueByID(final WebDriver driver, final String elementID, final String elementName) 
	{
		try {
			final JavascriptExecutor js = (JavascriptExecutor) driver;
		    return (String) js.executeScript("return document.getElementById(" + "'" + elementID + "'" + ").value");
		} catch (final org.openqa.selenium.TimeoutException ele) {
			LOGGER.error(ERROR_MSG, ele);
			fail();
		} 
		return null;
	}
	
	/**
	 * -------------------------------------------------------------------
	 * select value in PrimeNgDropdown
	 */
	
	/*public static boolean selectValueInPrimeNgDropDown(final WebDriver driver, final WebElement dropDownElementOrParent, final String Value) 
	{
		final WebElement DropDownElement = findlabelPrimeNgDropdown(driver, dropDownElementOrParent);
		final WebElement element = DropDownElement.findElement(By.xpath(String.format(DROPDOWN_ITEM_SELECTION_IN_OVERLAY, Value)));
		clickElementbyWebElement(driver, element);
	}*/

	/*public static void resizeWindowForDeviceType(String deviceType)
	{
		TestBase.setDeviceType(deviceType);
		TestBase.resizeWindowForDeviceType();
	}*/
	
}
