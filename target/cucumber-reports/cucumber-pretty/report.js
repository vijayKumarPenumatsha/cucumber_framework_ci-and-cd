$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/features/login/login_logout.feature");
formatter.feature({
  "line": 1,
  "name": "Farmers Loan Programe",
  "description": "",
  "id": "farmers-loan-programe",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "comments": [
    {
      "line": 6,
      "value": "#-------------------------------------------------------------------------------------------"
    }
  ],
  "line": 8,
  "name": "Farmers Loan Process Portal",
  "description": "",
  "id": "farmers-loan-programe;farmers-loan-process-portal",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 7,
      "name": "@Farmers_Loan_Process"
    }
  ]
});
formatter.step({
  "line": 9,
  "name": "User Able To Click On Programes \u0026 Services Drpdown",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "User Able To Navigate On Farm Bill Home",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "User Able To Click On Farm Loans",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "User Able To Click On Online Services",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "User Able To Click On FSAFarm",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "User Navigate To FSAFarm Login Page",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "User Enter The UserId \u003cUserId\u003e",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "User Enter The Password \u003cPassword\u003e",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "User Click On Submit Button",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "User Click On Create Account",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "User Click On Customer Radio Button",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "User Click On Continue Button",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "User Enter The Email Address \u003cEmail\u003e",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "User Click On Submit",
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "User Click On Update Account",
  "keyword": "And "
});
formatter.step({
  "line": 24,
  "name": "User Click On Continue To Login Page",
  "keyword": "And "
});
formatter.step({
  "line": 25,
  "name": "User Click On Manage Account Dropdown",
  "keyword": "And "
});
formatter.step({
  "line": 26,
  "name": "User Navigate To Change Password",
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "User Enter On The UserID \u003cUserId\u003e",
  "keyword": "And "
});
formatter.step({
  "line": 28,
  "name": "User Enter The Old Password \u003cPassword\u003e",
  "keyword": "And "
});
formatter.step({
  "line": 29,
  "name": "User Enter The On The Password \u003cPassword\u003e",
  "keyword": "And "
});
formatter.step({
  "line": 30,
  "name": "User Click On Submit Label",
  "keyword": "And "
});
formatter.examples({
  "line": 32,
  "name": "",
  "description": "",
  "id": "farmers-loan-programe;farmers-loan-process-portal;",
  "rows": [
    {
      "cells": [
        "UserId",
        "Password",
        "Email"
      ],
      "line": 33,
      "id": "farmers-loan-programe;farmers-loan-process-portal;;1"
    },
    {
      "cells": [
        "username",
        "password@123@",
        "abc@gmail.com"
      ],
      "line": 34,
      "id": "farmers-loan-programe;farmers-loan-process-portal;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 7124821000,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "I am on the Farm Service Agency Portal URL \"https://www.fsa.usda.gov/online-services/farm-plus/index\"",
  "keyword": "Given "
});
formatter.match({
  "arguments": [
    {
      "val": "https://www.fsa.usda.gov/online-services/farm-plus/index",
      "offset": 44
    }
  ],
  "location": "loginLogoutPageStepDefinitions.iAmOnTheFarmServiceAgencyPortalURL(String)"
});
formatter.result({
  "duration": 6657876800,
  "status": "passed"
});
formatter.scenario({
  "line": 34,
  "name": "Farmers Loan Process Portal",
  "description": "",
  "id": "farmers-loan-programe;farmers-loan-process-portal;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 7,
      "name": "@Farmers_Loan_Process"
    }
  ]
});
formatter.step({
  "line": 9,
  "name": "User Able To Click On Programes \u0026 Services Drpdown",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "User Able To Navigate On Farm Bill Home",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "User Able To Click On Farm Loans",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "User Able To Click On Online Services",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "User Able To Click On FSAFarm",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "User Navigate To FSAFarm Login Page",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "User Enter The UserId username",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "User Enter The Password password@123@",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "User Click On Submit Button",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "User Click On Create Account",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "User Click On Customer Radio Button",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "User Click On Continue Button",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "User Enter The Email Address abc@gmail.com",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "User Click On Submit",
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "User Click On Update Account",
  "keyword": "And "
});
formatter.step({
  "line": 24,
  "name": "User Click On Continue To Login Page",
  "keyword": "And "
});
formatter.step({
  "line": 25,
  "name": "User Click On Manage Account Dropdown",
  "keyword": "And "
});
formatter.step({
  "line": 26,
  "name": "User Navigate To Change Password",
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "User Enter On The UserID username",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 28,
  "name": "User Enter The Old Password password@123@",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 29,
  "name": "User Enter The On The Password password@123@",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 30,
  "name": "User Click On Submit Label",
  "keyword": "And "
});
formatter.match({
  "location": "loginLogoutPageStepDefinitions.userAbleToClickOnProgramesAndServicesDrpdown()"
});
formatter.result({
  "duration": 4170542400,
  "status": "passed"
});
formatter.match({
  "location": "loginLogoutPageStepDefinitions.userAbleToNavigateOnFarmBillHome()"
});
